Hi, I am Pavlo Kerestey.

I work as a software engineer
at [Global Access Internet Software GmbH](https://global.de) where I write
software that automates our data center
using [Haskell](https://www.haskell.org/). We have published several libraries,
of which I am maintainer for [NetSNMP](https://github.com/ptek/netsnmp)

Sometimes, I hang out on the freenode IRC as **ptek**. And you can <script>write_me_an_email()</script>
