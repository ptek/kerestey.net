Built with [hakyll](https://jaspervdj.be/hakyll/) and hosted
on [uberspace](https://uberspace.de).
Font [et-book](https://edwardtufte.github.io/et-book/). Images
by [unsplash](https://unsplash.com/).
