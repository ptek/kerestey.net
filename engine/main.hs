{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TupleSections     #-}

module Main where

import Hakyll
import System.FilePath

hakyllConf :: Configuration
hakyllConf = defaultConfiguration {
  providerDirectory = "./src/"
  ,deployCommand= "rsync --del -cr _site/* uberspace:/home/ptek/html/"
  }

main :: IO ()
main =
  hakyllWith hakyllConf $ do
    match "static/**" $ do
      route $ gsubRoute "static/" (const "")
      compile copyFileCompiler
    match "css/*" $ compile compressCssCompiler
    create ["style.css"] $ do
      route idRoute
      compile $ do
        csses <- loadAll "css/*.css"
        makeItem $ unlines $ map itemBody csses
    match "texts/*" $ compile pandocCompiler
    match "index.html" $ do
      route idRoute
      compile $ do
        -- homeText <- load "texts/home.md" >>= fmap itemBody . renderPandoc
        -- ackText <- load "texts/acknowledgements.md" >>= fmap itemBody . renderPandoc
        -- let indexContext =
        --       constField "content-home" homeText
        --       <> constField "acknowledgements" ackText
        textContext <- loadTexts
        getResourceBody >>= applyAsTemplate textContext >>= relativizeUrls

loadTexts :: Compiler (Context a)
loadTexts =
  loadAll "texts/*.md"
  >>= mapM (fmap toConstField . renderPandoc)
  >>= return . mconcat
  where
  toConstField Item{..} = constField (toFieldName itemIdentifier) itemBody
  toFieldName = ("text-" ++) . takeBaseName . toFilePath
